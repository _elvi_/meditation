package com.example.meditation.on_boarding_screen

data class OnBoardingItem(
    val image: Int,
    val title:String,
    val subTitle:String
)
