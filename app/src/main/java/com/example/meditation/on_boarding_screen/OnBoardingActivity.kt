package com.example.meditation.on_boarding_screen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.meditation.R
import com.example.meditation.databinding.ActivityOnBoardingBinding
import com.example.meditation.sign_up_screen.SignUpActivity

class OnBoardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnBoardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.viewPager.adapter = OnBoardingAdapter(this, Constants.onBoardingItemsList)

        binding.nextButton.setOnClickListener {
            if (binding.viewPager.adapter != null) {
                if (binding.viewPager.currentItem == binding.viewPager.adapter!!.itemCount - 1) {
                    startActivity(
                        Intent(this, SignUpActivity::class.java)
                    )
                    finish()
                } else
                    binding.viewPager.currentItem += 1
            }
        }

        binding.viewPager.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0 -> {
                        binding.currentPage.setImageResource(R.drawable.ic_current_first_page)
                        binding.nextButton.text = getString(R.string.stringNext)
                    }
                    1 -> {
                        binding.currentPage.setImageResource(R.drawable.ic_current_second_page)
                        binding.nextButton.text = getString(R.string.stringNext)
                    }
                    2 -> {
                        binding.currentPage.setImageResource(R.drawable.ic_current_third_page)
                        binding.nextButton.text = getString(R.string.stringLetsStart)
                    }
                }
            }
        })
        binding.skipButton.setOnClickListener {
            startActivity(
                Intent(this, SignUpActivity::class.java)
            )
            finish()
        }
    }
}