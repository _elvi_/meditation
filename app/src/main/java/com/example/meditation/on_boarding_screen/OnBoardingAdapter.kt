package com.example.meditation.on_boarding_screen

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meditation.R
import com.example.meditation.databinding.OnboardingPageBinding

class OnBoardingAdapter (private val context: Context, private val pages: List<OnBoardingItem>) : RecyclerView.Adapter<OnBoardingAdapter.ViewHolder>() {
    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.onboarding_page, parent, false))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val binding = OnboardingPageBinding.bind(holder.itemView)
        val page = pages[position]

        binding.imageView.setImageResource(page.image)
        binding.titleText.text = page.title
        binding.subtitleText.text = page.subTitle

    }
    override fun getItemCount() = pages.size
}