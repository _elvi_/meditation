package com.example.meditation.on_boarding_screen

import com.example.meditation.R

object Constants {
    val onBoardingItemsList = listOf(
        OnBoardingItem(
            R.drawable.onboarding_image_1,
            "Learn anytime and anywhere",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!"
        ),
        OnBoardingItem(
            R.drawable.onboarding_image_2,
            "Find a course for you",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!"
        ),
        OnBoardingItem(
            R.drawable.onboarding_image_3,
            "Improve your skills",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!"
        )
    )
}