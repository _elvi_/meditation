package com.example.meditation.sign_up_screen

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.meditation.databinding.ActivitySignUpBinding
import com.example.meditation.dictionary_screen.MainActivity

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.signUpButton.setOnClickListener() {
            val nameText = binding.nameButton.text.toString()
            val emailText = binding.emailButton.text.toString()
            val passwordText = binding.passwordButton.text.toString()

            if (nameText == SignUpItems.userName && emailText == SignUpItems.userEmail && passwordText == SignUpItems.userPassword) {
                val intent = Intent()
                intent.putExtra("correct name", nameText)
                intent.putExtra("correct email", emailText)
                intent.putExtra("correct password", passwordText)
                startActivity((Intent(this, MainActivity::class.java)))
            } else {
                Toast.makeText(this, "Something wrong", Toast.LENGTH_SHORT).show()
            }
        }
    }
}