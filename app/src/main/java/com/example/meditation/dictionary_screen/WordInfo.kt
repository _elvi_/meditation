package com.example.meditation.dictionary_screen

data class WordInfo(
    val word: String,
    val phonetic: String,
    val meanings: List<Meanings>
)
