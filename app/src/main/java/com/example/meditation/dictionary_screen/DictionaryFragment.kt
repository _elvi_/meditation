package com.example.meditation.dictionary_screen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.meditation.R
import com.example.meditation.databinding.FragmentDictionaryBinding
import com.example.meditation.databinding.WordExplainingItemBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DictionaryFragment : Fragment(R.layout.fragment_dictionary) {

    private lateinit var binding: FragmentDictionaryBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentDictionaryBinding.bind(view)

        binding.wordExplainingRecyclerView.layoutManager = LinearLayoutManager(context)

        binding.editText.setOnClickListener() {
            val text = binding.editText.text.toString()
            if (text.isNotEmpty()) {
                binding.placeholderFragment.visibility = View.GONE
                binding.dictionaryFragment.visibility = View.VISIBLE

                NetworkManager.instance
                    .getWordInfo(text)
                    .enqueue(object : Callback<List<WordInfo>> {
                        override fun onResponse(
                            call: Call<List<WordInfo>>,
                            response: Response<List<WordInfo>>
                        ) {
                            if (response.body() != null) {
                                val word = response.body()!![0]

                                binding.word.text = word.word
                                binding.phonetic.text = word.phonetic
                                binding.partOfSpeech.text = word.meanings[0].partOfSpeech

                                binding.wordExplainingRecyclerView.adapter = MeaningsAdapter (
                                    requireContext(),
                                    word.meanings[0].definitions
                                )
                            }
                        }

                        override fun onFailure(call: Call<List<WordInfo>>, t: Throwable) {
                            t.printStackTrace()
                        }
                    })

            }
        }
    }
}