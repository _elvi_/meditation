package com.example.meditation.dictionary_screen

data class Definitions(
    val definition: String,
    val example: String,
)