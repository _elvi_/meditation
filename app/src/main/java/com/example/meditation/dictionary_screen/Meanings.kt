package com.example.meditation.dictionary_screen

data class Meanings(
    val partOfSpeech: String,
    val definitions: List<Definitions>
)
