package com.example.meditation.dictionary_screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.meditation.R
import com.example.meditation.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_dictionary -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, DictionaryFragment())
                        .commit()
                }
                R.id.action_video -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, VideoFragment())
                        .commit()
                }
            }
            true
        }

        //оставляем, т.к. при открытии активки, что-то нужно показывать
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, DictionaryFragment())
            .commit()
    }
}