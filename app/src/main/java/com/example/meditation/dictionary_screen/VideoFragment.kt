package com.example.meditation.dictionary_screen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.meditation.R
import com.example.meditation.databinding.WebViewFragmentBinding

class VideoFragment : Fragment(R.layout.web_view_fragment) {

    private lateinit var binding: WebViewFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = WebViewFragmentBinding.bind(view)

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl("https://learnenglish.britishcouncil.org/general-english/video-zone")
    }
}