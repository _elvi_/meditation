package com.example.meditation.dictionary_screen

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meditation.R
import com.example.meditation.databinding.WordExplainingItemBinding

class MeaningsAdapter(
    private val context: Context,
    private val definitions: List<Definitions>
) : RecyclerView.Adapter<MeaningsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.word_explaining_item, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = WordExplainingItemBinding.bind(holder.itemView)
        val definition = definitions[position]

        binding.wordMeaning.text = definition.definition
        binding.wordExample.text = definition.example
    }

    override fun getItemCount() = definitions.size
}