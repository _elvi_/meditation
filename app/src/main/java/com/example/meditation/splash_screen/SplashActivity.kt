package com.example.meditation.splash_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.meditation.databinding.ActivitySplashBinding
import com.example.meditation.on_boarding_screen.OnBoardingActivity

class SplashActivity : AppCompatActivity() {

private  lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler().postDelayed({
            startActivity(
                Intent(this, OnBoardingActivity::class.java)
            )

            finish()
        }, 3000)
    }
}